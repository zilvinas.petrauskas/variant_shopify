function AnimEntryLogo(_config) {

    var LOOP_START_FRAME = 55; // if 30fps, then 25 frame is 25/30 -> 0.83 of a second
    var LOOP_END_FRAME = 155;

    /*
        TODO
        loop in the middle
        end animation - shrink

    */

    var FORWARD = 1; // danimate forward
    var BACKWARDS = -1; // reverse animation

    var scope = this;
    this.config = {
        maxSpeed: 4,
    }
    this.id = 'entry';
    this.file = 'variant-loader-data.json';
    this.animationData = null;
    this.animation = null;
    this.playing = true;
    this.speed = 1; // current animation speed
    this.raf = null; // requestAnimationFrame
    this.direction = FORWARD;
    this.debug = false;
    this.currentFrame = 0;
    this.slider = {};

    if (_config && Object.keys(_config).length > 0) {
        for (var c in _config) {
            if (this.hasOwnProperty(c)) {
                this[c] = _config[c];
            }
        }
    }

    this.init = function () {

    
        this.animation = lottie.loadAnimation({
            container: document.getElementById(this.id),
            renderer: 'canvas',
            loop: false,
            autoplay: false, // FALSE ALWAYS
            animationData: this.animationData
        });

        this.createEvents();

        if(this.debug === true) this.createDebugControls();
        
        this.play();

        window.addEventListener('loop:stop', function () {
            scope.end();
        });

        window.addEventListener('resize', function () {

        });
    }

    this.createEvents = function(){

        this.animation.addEventListener("enterFrame", function (animation) {

            scope.currentFrame = animation.currentFrame;

            if(scope.debug === true){
                document.dispatchEvent(new CustomEvent("slider:update", {detail: animation } ));
            }
            
            if (scope.direction === FORWARD && animation.currentTime >= LOOP_END_FRAME) {
                scope.direction = BACKWARDS;
                scope.animation.setDirection(scope.direction);
            } else if (scope.direction === BACKWARDS && animation.currentTime <= LOOP_START_FRAME) {
                scope.direction = FORWARD;
                scope.animation.setDirection(scope.direction);
            }
        });

    }


    this.play = function () {

        this.playing = true;
        this.animation.play();

    }

    this.pause = function(){
        this.playing = false;
        this.animation.pause();
    }

    this.stop = function () {
        this.playing = false;
        this.animation.stop();
    }

    this.end = function(){

        let elem = document.getElementById(this.id);
        elem.classList.add('end-entry-animation');

        setTimeout(function(){
            scope.stop();
        }, 3000);
    }

    this.setDirection = function (_direction) {
        this.animation.direction(_direction);
    }

    this.createDebugControls = function(){

        this.slider.text = document.getElementById('currentTime');
        this.slider.userInteracted = true;

        console.log('check animation', this.animation);

        const currentFrame = this.animation.currentFrame;
        const totalFrames = this.animation.totalFrames;

        document.addEventListener('slider:update', function(event){
            scope.slider.text.innerHTML = parseInt(event.detail.currentTime);
        });


        let play = document.getElementById('play');
        let stop = document.getElementById('stop');
        let end = document.getElementById('end');

        play.addEventListener('click', function(){
            scope.play();
        });

        stop.addEventListener('click', function(){
            scope.pause();
        })
        
        end.addEventListener('click', function(){
            scope.end();
        })

    }


    getAnimationJsonData(this.file, function (data) {
        scope.animationData = data;
        scope.init();
    });

    return this;
}


