var videoReady = function(elementId, onSuccess, onError){

    this.ready = false;
    this.onSuccessCallback = typeof onSuccess === 'function' ? onSuccess : null;
    this.onErrorCallback = typeof onError === 'function' ? onError : null;
    var scope = this;
    var vid = document.getElementById(elementId);
    var playOnLoad = false;

    function check(){

        if(typeof vid === 'undefined' || vid === null){
            if(scope.onErrorCallback !== null) scope.onErrorCallback();
            return;
        }
        console.log('vid src?', elementId, vid.src);
        vid.load(); // load video manually (fixes hard refresh bug, when video loads after a delay)
        vid.oncanplay = function() {
            if( ! scope.ready ) {
                if( ! playOnLoad) vid.pause();
                scope.ready = true;
                if(scope.onSuccessCallback !== null) scope.onSuccessCallback();
            }
        };
        vid.onerror = function(err) {
            if(!scope.ready){
              if(scope.onErrorCallback !== null) scope.onErrorCallback(err);
            }
        };

    }

    function play(){
        vid.classList.add('visible');
        vid.play();
    }

    function playWhenLoaded(){
        playOnLoad = true;
    }

    function isReady(){
        return scope.ready;
    }

    function getVideoContainer(){
        return vid;
    }


    return {
        check : check,
        play: play,
        ready: isReady,
        playWhenLoaded: playWhenLoaded,
        getVideoContainer: getVideoContainer
    }

}