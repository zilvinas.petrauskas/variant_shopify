(function(){

  var header = document.getElementById('site-header');
  var transparentClass = 'header-transparent';
  

  window.addEventListener('load', function(e){
    onPageScroll(); // make sure things are ready
  });
  window.addEventListener('scroll', function(e){
    onPageScroll(e);
  });
   window.addEventListener('touchmove', function(e){
    onPageScroll(e);
  });

  function onPageScroll(event){
    
    var posY = scrollPosition();
    
    if(posY <= 86){
      if( ! header.classList.contains(transparentClass)) header.classList.add(transparentClass);
    }else{
      if( header.classList.contains(transparentClass)) header.classList.remove(transparentClass);
    }
    
  }

  function scrollPosition(){
    return window.scrollY || window.scrollTop || document.getElementsByTagName("html")[0].scrollTop;
  }

})();

