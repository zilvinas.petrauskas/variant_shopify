(function () {

    window.addEventListener('load', function () {
        
        changeHomeVideoSrc('video-1', function () {
            changeHomeVideoSrc('video-2', function () {
                resizeInner();
                runProgress();
            })
        });

        try {
            document.getElementById('stopLoop').addEventListener('click', function () {
                window.dispatchEvent(new Event('loop:stop'));
            });
        } catch (e) {
            // this only exists locally
        }
    });

    function resizeInner(){
        if(window.innerWidth < 768){
            document.getElementById('new-progress').style.height = window.innerHeight+'px';
        }
    }

    function runProgress() {

        // start progress bar
        progress = new ProgressBar();
        progress.onComplete = function () {
            window.dispatchEvent(new Event('loop:stop'));
            setTimeout(function(){
                showPage();
            }, 750); 
        }
        progress.startLoop();

        // run home video preload
        homeVideoProgress();

        // start svg animation
        var entryAnim = new AnimEntryLogo({
            id: 'loaderAnimation',
            file: INTRO_ANIM_DATA,
        });

    }

    function showPage() {
      
        playVideos();

        // animate with black blocks and show page
        black_bar_animation(function(){
            hideProgressWrapper();
        });

    }

    function hideProgressWrapper(){
        // hide insides of progress bar
        
        var progressWrapper = document.getElementById('loading-progress-wrapper');

        var transitioned = setTimeout(function(){
            progressWrapper.style.display = 'none';
        }, 1500);

        progressWrapper.addEventListener('transitionend', function () {
            progressWrapper.style.display = 'none';
            clearTimeout(transitioned);
        })
        progressWrapper.style.opacity = 0;
    }

    function playVideos() {
        document.getElementById('video-1').play();
        setTimeout(function () {
            document.getElementById('video-2').play();
        }, 777);
    }

    
    function homeVideoProgress() {

        var total = 2;
        var ready = 0;

        var options = { videoCanPlay: 'loadedmetadata' };

        var video1 = new videoReady('video-1', function () {
            console.log('video1 ready');
            videoLoadedAndReady();
        }, function () {
            console.log('video-1 failed to load');
        }, options);
        video1.check();

        var video2 = new videoReady('video-2', function () {
            console.log('video2 ready');
            videoLoadedAndReady();
        }, function () {
            console.log('video-2 failed to load');
        }, options);
        video2.check();

        function videoLoadedAndReady() {
            ready += 1;
            progress.addOnVideoLoad();
            if (ready >= total) {
                console.log('all videos ready');
                progress.finish();
            }
        }
    }

})();


function black_bar_animation(callback) {



    //black blocks animation - final one on loading
    var anArray = $(".black_bar_container").find(".black_bar");
    var delay = 0;
    for (var i = 0; i < anArray.length; i++) {
        var $v = $(anArray[i]);

        delay += 40;
        delayedDrop($v, delay);

        if(i===anArray.length-1){
            lastElm = $v;
        }

        // $v.delay(40 * i).queue(function () {
            // $(this).addClass("black_down");
        // });
    }
    anArray.last().on('transitionend', function () {
        // console.log('black completed');
        if (callback) callback();
    });
   

    function delayedDrop($elm, _delay){
        setTimeout(function(){
            $elm.addClass('black_down');
            // console.log('black down', _delay);
        }, _delay)
    }
    
};


function changeHomeVideoSrc(id, callback) {

    var isDesktop = window.innerWidth > 1024;
    var vid = document.getElementById(id);
    console.log('vid?', id, vid);
    var source = vid.getElementsByTagName('source')[0];
    console.log('source?', source);
    var src_desktop = source.dataset['desktopSrc']; // desktop-src
    var src_mobile = source.dataset['mobileSrc']; // mobile-src
    var src_current = source.src;

    if (isDesktop) {
        if (src_current != src_desktop) {
            console.log('desktop video', id, src_desktop);
            source.setAttribute('src', src_desktop);
            if (callback) callback();
        }
    } else {
        if (src_current != src_mobile) {
            console.log('mobile video', id, src_mobile);
            source.setAttribute('src', src_mobile);
            if (callback) callback();
        }
    }



}






var ProgressBar = function () {
    var scope = this;
    this.currentProgress = 0;
    this.delayedProgress = 34; // +33 +33 = 100
    this.done = false;
    this.loop = undefined; // animation frame
    this.element = document.getElementById('progress-bar');
    this.parent = document.querySelectorAll('.loading-progress-bar')[0];
    this.video = document.getElementById('loadingVideo');

    this.startLoop = function () {
        this.loop = requestAnimationFrame(() => {
            this.animate(true);
        });
    }

    this.add = function (value) {
        let newProgress = Math.min(100, this.delayedProgress + value); // make sure its not above 100
        this.set(newProgress);
    }

    this.addOnVideoLoad = function () {
        this.add(33);
    }

    this.set = function (progress) {
        if (progress > this.delayedProgress && progress > 0 && progress <= 100) {
            this.delayedProgress = progress;
        }
    }

    this.animate = function (loop) {
        if (!this.done) {
            if (this.currentProgress < this.delayedProgress) {
                // if (this.delayedProgress - this.currentProgress > 5) {
                //     this.currentProgress += 5;
                // } else if (this.delayedProgress - this.currentProgress > 3) {
                //     this.currentProgress += 3;
                // } else {
                //     this.currentProgress += 1;
                // }
                this.currentProgress += 1;
                this.currentProgress = Math.min(100, this.currentProgress); // cap at 100
                this.element.style.width = this.currentProgress + '%';
            } else if (this.currentProgress >= 100) {
                this.done = true;
                this.cancelLoop();
                this.hideBar();
                this.onComplete();
            }
            if (loop === true) {
                this.loop = requestAnimationFrame(() => {
                    this.animate(loop);
                });
            }
        }
    }

    this.hideBar = function () {

        var parent = this.parent;

        setTimeout(function(){
            parent.style.opacity = 0;
        }, 200); // for video to transition

    }

    this.finish = function () {
        this.delayedProgress = 100;
        this.animate();
    }

    this.cancelLoop = function () {
        cancelAnimationFrame(this.loop);
    }

    this.onComplete = function () { }
}