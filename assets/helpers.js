function getAnimationJsonData(url, callback){

    getJSON(url, function(response){
        console.log('status', status);
        console.log('response', response);
        callback(response);
    }, function(error, status){
        console.warn('Error', status, error);
    })

}

var getJSON = function(url, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        success(xhr.response);
      } else {
        error(xhr.response, status);
      }
    };
    xhr.send();
};

function getElementHeight(elemQuery) {
  var elem = document.querySelectorAll(elemQuery);
  var h = elem[0].innerHeight || elem[0].offsetHeight;

  console.log(elemQuery, h, elem);
  return h;
}